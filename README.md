19A ITT1 Project
================

# Background

The project I about how to charge a robot wireless.
The plan is the we are going to find out, how to make it work, on a theory level and in practice.
With the help of the 3 semesters student, we are building a wireless station, so a robot can charge its battery.
The project was giving to us from a company in Copenhagen, named Copenhagen Robotic.
Then giving to the school as a project for the 3 semesters, and with the help from us the first semester. 
# Purpose

The purpose of this project is to learn how to do group projects and how to structure work. We learn how to use gitlap to manage group projects with.

As part of this learning project we have to create a wireless charging prototype. In that we learn how the electronic behind it works and how to share this knowledge with the rest of the group.

# Goals

![Image of goals](Documentation/Goals.JPG "Image of goals")

* Phase 1: Complete the Idea and Theory.

* Phase 2: Complete the Breadboard prototype.

* Phase 3: Final documentation and prototype.

* Phase 4: Report + Print


Project deliveries are:
* A Prototype 
* Full report

# Schedule

The project is divided into 4:

* Phase 1: Idea and research, in this time line week 38 to week 40 we do the idea and research.
* Phase 2: Development, where we test the early prototypes and lab test its from week 41-45
* Phase 3: Consolidation, where we bringing the phase 1 and 2 together and prepare for the prototype build in phase 4 week 46-47.
* Phase 4: Is the prototype build and will include the functionality chosen in phase 1 and the result in phase 2. From week 48-50 and last its the finalize report in week 51 and week 2 is project presentation

# Organization

Steering committee
* Ilias Esmati
* Nikolaj Peter Esbjørn Simonsen
* Per Dahlstrøm

Project manager
* Alexander With

Composition of the project group 1st semester
* Alexander With
* Andrea Pantieri,
* Nicolai Skytte,
* Marcus Ziegler,
* Mohammed,
* Barnabas Laczko,
* Adam Dolinajec,
* Fadi Salem
* Adham Ahmad

Composition of the project group 3rd semester
* Tihamer Biliboc,
* Karthiebhan Mahendran,
* Anthony Peak,
* Henry Valdovskis,
* Laurynas Medvedevas,
* Sebastian Mason

External Resource
* At the moent we don't have any externals resources. This will be updated when a resource is aquired.


# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute.


# Risk assessment

For this project, there are several risks which can have an impact on the project,
the worst of all is that the whole project is cancelled from the company, 
which would leave us to figure out both how it was ment to work and figure out what made it fail,
in other words we would need to make a post-mortum analyses.

The next worst thing to happen would be if several member of the groups left the group or stop showing up and communicating.
this would force us to firstly review what tasks they were working on and decide how important those tasks are to the rest of the project,
after that we would need to figure out if we will have to adjust the scope of the project,
even if it's just a learning porcess it would come in handy knowing how to react to things like this in the real world.
amoung other risk is that we might feel that the final report need to be tuned more before hand-in,
but that is already taking into account has we already have setup an "fake" deadline which we hope to have the report ready for.

if we experiance technical issue with the our prototype we have different options depending on what stage we are with it, if we have trouble at the breadboard lvl it's possible we are setting thing up in a wrong way, or that we are lacking the components that we need to make things work as intended, for this we are using an software called "fritzing" which allows us to make a circuit map/plan form which we can build the prototype. on a breadboard lvl we can change the setup several times before we move over to the next step which is setting it up on a protoboard. on the proto ´board we will need to be a little more careful, esspecially as we need to solder the component to the board and make sure the connections are right or we risk damaging the components and having to order replacements which will take and delay testing the setup before we acan order a print of the final setup of the prototype. which will an probably have long delivary time.

under this whole process we might seek guidance from other who have worked with it before, if all else fails for the prototype we still have the circuit map to show how we would intent the physical prototype to work.


# Stakeholders


the stake-holder is this project is primarily the company
who requesting an solution to this problem, as they are putting
time and resources into finding a method to inspect the 
charging stations for the cars who are going to use them.
so they are primary the loser here, after them are the thrid
semester students who are doing most of the reporting and 
developing for the company, and the we the 1st semesters
have the least to loss, as it more of learning experiance
for us. however if the project ends with good results
then the company who has invested in´it will have the 
possiblity to get either get ahead on a market or sell this as
a solution to potential customers

# Communication


Since the project is started by an company and it's also being work on by another group[3rd semesters], we need to be able to communicate with first the other group and secondary the company.

However it's the company who dictates what infomation we can share between us and people outside the project. the project is also being guided the teachers we cordinates things in the beginning. 

We have weekly meeting with both the teachers and the other group, we have yet to know how we should get in touch with the company behind the project.

For the project group, we use multiple tools to staying in touch and staying updated.
 * For project managment we use Gitlab
 * For communication between members we use a discord for message-based communication
 * Email for notifications on what's happening on the project.


# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

* Title Page 
* List of acronyms and abbreviations
* Table of contents, including list of annexes
* Introduction: background and context of the programme
* Description of the program – its logic theory, results framework and external factors likely to affect success
* Purpose of the evaluation
* Key questions and scope of the evaluation with information on limitations and de-limitations
* Methodology
* Findings
* Conclusions 
* Recommendations 
* Lessons, generalizations, alternatives

# References


[How Does Wireless Charging Work? || Crude Wireless Energy Transfer Circuit](https://www.youtube.com/watch?v=ExU32UyGX6w)

[DIY Wireless Energy Transfer System](https://www.youtube.com/watch?v=3E5PUnYlaTM)

[Introduction](https://docs.google.com/presentation/d/1gKS9Mw4nWfEJnkdrwEexPpExaU0urh0Apz38lcwq6TY/edit#slide=id.p)


[Create a project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

[Nikolaj Simonsen Gitlab](https://gitlab.com/npes)

[Elias Gitlab project](https://gitlab.com/ilias-gitlab)

[Performing a Project Premortem](https://hbr.org/2007/09/performing-a-project-premortem)

[Learn Issues for gitlab](https://docs.gitlab.com/ee/user/project/issues/)

[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[How To Create SMART tasks](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

[CURRICULUM or the IT Technology Programme](https://www.ucl.dk/globalassets/01.-uddannelser/a.-videregaende-uddannelser/internationale-uddannelser/it-technology-ap/dokumenter/curriculum-2018-it-technology.pdf?nocache=0065f079-10eb-4d06-bbd4-18fd7178b2f5)


