## How to use the issues

At first, find an issue that needs to be worked on. When the issue isn't taken, asign yourself to the issue.
 Click on "Assign Yourself" at the top right.

The issue should have tasks that should be ticked off, before you are done. If anything else is needed for the issue, edit the issue at hand.

When the issue is completed, add a "review" label to the issue. Then another will look at the work and assest if you have completed the work.

If you review a issue, make sure that the task is completed. If it is, remove the "review" label and close the issue. 
